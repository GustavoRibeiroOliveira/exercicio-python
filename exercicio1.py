# Exercicio 1

numero1 = float(input("Digite o primeiro número: "))
numero2 = float(input("Digite o segundo número: "))

if numero1>numero2:
    print("O primeiro número é maior.")
elif numero1<numero2:
    print("O segundo número é maior.")
else:
    print("Os números são iguais.")

# Exercicio 2

percentualCrescimento = float(input("Digite o percentual de crescimento sem a %: "))

if percentualCrescimento > 0:
    print("A produção da empresa teve um aumento.")
elif percentualCrescimento < 0:
    print("A produção da empresa não teve um aumento.")
else:
    print("A produção da empresa permaneceu a mesma.")