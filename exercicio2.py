# Exercicio 1

numeroInt1 = int(input("Digite o primeiro número: "))
numeroInt2 = int(input("Digite o segundo número: "))

if numeroInt1>numeroInt2:
    for x in range(numeroInt2+1,numeroInt1):
        print(x)
elif numeroInt1<numeroInt2:
    for x in range(numeroInt1+1,numeroInt2):
        print(x)
else:
    print("Os números são iguais.")

# Exercicio 2

numeroTabuada = int(input("Digite um número: "))

for contadorTabuada in range(1,11):
    # print(contadorTabuada, " * ", numeroTabuada, " = ",contadorTabuada*numeroTabuada)
    print(f"{contadorTabuada} * {numeroTabuada} = {contadorTabuada*numeroTabuada}")
