listaCarros = []

while True:
    print("\n\n-- Loja de Veículos --")
    print("1 - Listar carros")
    print("2 - Comprar carro")
    print("3 - Cadastrar carro")
    print("4 - Sair")

    choice = int(input("Digite a opção: "))

    if choice == 1:
        print(listaCarros)
    elif choice == 2:
        print(listaCarros)
        nCarro = int(input("Digite o numero da posição do carro que deseja comprar na lista: "))
        listaCarros.pop(nCarro-1)
    elif choice == 3:
        marca = input("Digite a marca do carro: ")
        modelo = (input("Digite o modelo do carro: "))
        preco = float(input("Digite o preço do carro: "))
        listaCarros.append({"marca": marca, "modelo": modelo, "preco": preco})
        print("Carro adicionado.")
    elif choice == 4:
        break
    else:
        print("Opção inválida!")