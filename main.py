# print("Hello World")
#
# media = float(input("Digite a média: "))
#
# if media>=6:
#     print("Aprovado!")
# elif 6 > media >= 4:
#     print("NP3!")
# else:
#     print("Reprovado!")

# t1 = t2 = True
# f1 = f2 = False
#
# if t1 and f2:
#     print("expressão verdadeira")
# else:
#     print("expressão falsa")
#
# if t1 or f2:
#     print("expressão verdadeira")
# else:
#     print("expressão falsa")
#
# if not f1:
#     print("expressão verdadeira")
# else:
#     print("expressão falsa")


# lista = "Leandro, Guilherme, João, Pedro, Ana"
#
# nome_1 = "Leandro"
# nome_2 = "Jose"
#
# if nome_2 in lista:
#     print(f"{nome_2} está na lista")
# else:
#     print(f"{nome_2} não está na lista")


# cont = 1

# while cont <= 3:
#     nota_1 = float(input("Digite a primeira nota: "))
#     nota_2 = float(input("Digite a segunda nota: "))
#
#     print(f"Média: {(nota_1 + nota_2) / 2}")
#
#     cont += 1

# for cont in range(1,11):
#     print(cont)

# for cont in range(1,4):
#     nota_1 = float(input("Digite a primeira nota: "))
#     nota_2 = float(input("Digite a segunda nota: "))
#
#     print(f"Média: {(nota_1 + nota_2) / 2}")

# lista = ["Fabricio", 9.5, 9.0, 8.0, True]
# print(lista)
#
# print(lista[0])

# for elemento in lista:
#     print(elemento)

# lista[3] = 10.0

# for elemento in lista:
#     print(elemento)

# media = (lista[1] + lista[2] + lista[3]) / 3
# print(f"Média: {media}")

# print(f"Quantidade de elementos na lista: {len(lista)}")

# lista.append(media)
# print(lista)
#
# lista.extend([10.0, 8.0, 9.0])
# print(lista)


# del lista[1:4]
# lista.pop(2)

# item_list = ['item', 5, 'foo', 3.14, True]
# list_to_remove = ['item', 5, 'foo']
#
# final_list = list(set(item_list) - set(list_to_remove))

# lista.remove(10.0)
# print(lista)


# dicionario = {
#     "chave1" : 1,
#     "chave2" : 2
# }
#
# print(dicionario)

# cadastro = {
#     "matricula": 84646464,
#     "dia_cadastro": 25,
#     "mes_cadastro": 10,
#     "turma": "2E"
# }

# print(cadastro["matricula"])
# print(cadastro["turma"])

# cadastro["turma"] = "2G"
# print(cadastro)
#
# cadastro["modalidade"] = "EAD"
# print(cadastro)
#
# cadastro.pop("turma")
# print(cadastro)
#
# print(cadastro.items())
# print(cadastro.keys())
# print(cadastro.values())
#
# for chaves in cadastro.keys():
#     print(cadastro[chaves])
#
# for chaves,valores in cadastro.items():
#     print(chaves, valores)


# def media():
#     nota_1 = float(input("Digite a primeira nota: "))
#     nota_2 = float(input("Digite a segunda nota: "))
#
#     print(f"Média: {(nota_1+nota_2)/2}")
#
# while True:
#     print("1 - Calcular Média")
#     print("2 - Sair")
#
#     choice = int(input("Digite a opção: "))
#
#     if choice == 1:
#         media()
#     elif choice == 2:
#         break
#     else:
#         print("Opção inválida!")


lista = [
    {"aluno":"davi","nota_1":10,"nota_2":8},
    {"aluno":"Leandro","nota_1":7,"nota_2":9}
]

# print(lista[0]["aluno"])
print(lista)

aux = int(input("Digite quantos alunos serão adicionados: "))

for count in range(1, aux+1):
    aluno = input("Digite o nome do aluno: ")
    nota_1 = float(input("Digite a primeira nota do aluno: "))
    nota_2 = float(input("Digite a segunda nota do aluno: "))

    lista.append({"aluno":aluno,"nota_1":nota_1,"nota_2":nota_2})
    print("Aluno adicionado.")

print(lista)